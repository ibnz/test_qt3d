#include <QApplication>
#include <QEntity>
#include <QMesh>
#include <QSceneLoader>
#include <Qt3DCore/QTransform>
#include <Qt3DCore/QNode>
#include <Qt3DRender/QPointLight>
#include <Qt3DInput/QInputAspect>
#include <Qt3DRender/QCamera>
#include <QAttribute>
#include <QBuffer>
#include <QDiffuseMapMaterial>
#include <QFirstPersonCameraController>
#include <QPhongMaterial>
#include <QTextureImage>
#include <Qt3DWindow>
#include <Qt3DExtras/QForwardRenderer>
#include <QFileInfo>
#include <QMessageBox>

void setupView(Qt3DExtras::Qt3DWindow *view, Qt3DCore::QEntity *rootEntity){
    view->defaultFrameGraph()->setClearColor(QColor(QRgb(0x4d4d4f)));
    Qt3DInput::QInputAspect *input = new Qt3DInput::QInputAspect;
    view->registerAspect(input);



    // Camera
    Qt3DRender::QCamera *cameraEntity = view->camera();

    cameraEntity->lens()->setPerspectiveProjection(45.0f, 16.0f/9.0f, 0.1f, 1000.0f);
    cameraEntity->setPosition(QVector3D(0, 0, 20.0f));
    cameraEntity->setUpVector(QVector3D(0, 1, 0));
    cameraEntity->setViewCenter(QVector3D(0, 0, 0));

    Qt3DCore::QEntity *lightEntity = new Qt3DCore::QEntity(rootEntity);

    Qt3DRender::QPointLight *light = new Qt3DRender::QPointLight(lightEntity);
    light->setColor("white");
    light->setIntensity(1);
    lightEntity->addComponent(light);
    Qt3DCore::QTransform *lightTransform = new Qt3DCore::QTransform(lightEntity);
    lightTransform->setTranslation(cameraEntity->position());
    lightEntity->addComponent(lightTransform);

    // For camera controls
    Qt3DExtras::QFirstPersonCameraController *camController = new Qt3DExtras::QFirstPersonCameraController(rootEntity);
    camController->setCamera(cameraEntity);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    Qt3DExtras::Qt3DWindow view;
    Qt3DCore::QEntity *rootEntity = new Qt3DCore::QEntity();
    setupView(&view, rootEntity);

//    QUrl url = QUrl::fromLocalFile("c:/Qt/Qt5.8.0/Examples/Qt-5.8/qt3d/exampleresources/assets/chest/Chest.obj");
//    QUrl url = QUrl::fromLocalFile("c:/Users/Alex-pc/Documents/prj/3d/stationBus.OBJ");
//    QUrl url = QUrl::fromLocalFile("c:/Users/Alex-pc/Documents/prj/3d/m/машина_без_всего.obj");
    QUrl url = QUrl::fromLocalFile("c:/blender/1.fbx");

    if (argc > 1){
        url = QUrl::fromLocalFile(argv[1]);
    }
    Qt3DCore::QTransform *tr = new Qt3DCore::QTransform();
    qreal scale = 1E-0f;
    if (argc > 2){
        scale = exp(QString(argv[2]).toInt());
    }
    tr->setScale(scale);

    if (!QFileInfo::exists(url.toLocalFile()))
    {
        QMessageBox::critical(0, QObject::trUtf8("Файл не найден: "), url.toLocalFile());
        return -1;
    }

//    QUrl url = QUrl::fromLocalFile("c:/Users/Alex-pc/Documents/prj/3d/кирпичи.3DS");
//    QUrl url = QUrl::fromLocalFile("c:/Users/Alex-pc/Documents/prj/3d/square-pot.obj");
//    QUrl url = QUrl::fromLocalFile("c:/Users/Alex-pc/Documents/prj/3d/fork.3DS");
    Qt3DRender::QSceneLoader *loader = new Qt3DRender::QSceneLoader(rootEntity);
    rootEntity->addComponent(loader);
    //        QObject::connect(loader, &Qt3DRender::QSceneLoader::statusChanged,
    //                         &app, [](Qt3DRender::QSceneLoader::Status s){qDebug() << s;});
    loader->setSource(url);
    QObject::connect(loader, &Qt3DRender::QSceneLoader::statusChanged, loader,
            [rootEntity](Qt3DRender::QSceneLoader::Status status){
                if (status == Qt3DRender::QSceneLoader::Ready){
                    QMessageBox::information(0, QObject::trUtf8("Загрузка завершена "), QObject::trUtf8("Все ок"));
                }
                auto nodes = rootEntity->childNodes();;
                qDebug() << nodes;
                for (auto &i : nodes) {
                    Qt3DCore::QEntity *ent = qobject_cast<Qt3DCore::QEntity *>(i);
                    if (ent){
                        if (ent->objectName() == "машина_без_всего.obj")
                            qDebug() << ent->childNodes();
                    }
                }
            }
            );

    Qt3DRender::QMesh *mesh = new Qt3DRender::QMesh();
    mesh->setSource(url);

//    QObject::connect(mesh, &Qt3DRender::QMesh::geometryChanged,
//                     &app,
//        [mesh]()
//        {
//            qDebug() << "loaded";
//            qDebug() << "childNodes " << mesh->childNodes();
//            qDebug() << "entities " << mesh->entities();
//            qDebug() << "instanceCount " << mesh->instanceCount();
//            qDebug() << "meshName " << mesh->meshName();
//            qDebug() << "source " << mesh->source();
//            qDebug() << "\\\\\\\\\\\\\\\\\\\\\\\\GEOMETRY/////////////////////////////";
//            Qt3DRender::QGeometry *g = mesh->geometry();
//            qDebug() << "boundingVolumePositionAttribute " << g->boundingVolumePositionAttribute();
//            foreach (auto a, g->attributes()) {
//                qDebug() << a->name();
//            }
//        });
//    Qt3DRender::QGeometry *g = mesh->geometry();
//    QObject::connect(mesh->geometry(), &Qt3DRender::QGeometry::boundingVolumePositionAttributeChanged,
//                     &app,
//        [g]()
//        {
//            qDebug() << "boundingVolumePositionAttribute " << g->boundingVolumePositionAttribute();
//        });




//    auto *material = new Qt3DExtras::QPhongMaterial();
//    material->setDiffuse(QColor(QRgb(0x665423)));

    Qt3DExtras::QDiffuseMapMaterial *diffuseMapMaterial = new Qt3DExtras::QDiffuseMapMaterial();
    diffuseMapMaterial->setSpecular(QColor::fromRgbF(0.2f, 0.2f, 0.2f, 1.0f));
    diffuseMapMaterial->setShininess(2.0f);

    Qt3DRender::QTextureImage *chestDiffuseImage = new Qt3DRender::QTextureImage();
    chestDiffuseImage->setSource(QUrl::fromLocalFile("c:/Users/Alex-pc/Documents/prj/3d/021f66b634628d2d3d383ae54888ecfc.jpg"));
    diffuseMapMaterial->diffuse()->addTextureImage(chestDiffuseImage);

//    Qt3DCore::QEntity *meshEntity = new Qt3DCore::QEntity(rootEntity);
//    meshEntity->addComponent(mesh);
//    meshEntity->addComponent(tr);
//    meshEntity->addComponent(diffuseMapMaterial);
    rootEntity->addComponent(tr);
    view.setRootEntity(rootEntity);
    view.show();

    return app.exec();
}
